const { Schema, model } = require("mongoose");
const bcrypt = require('bcryptjs');
const validator = require('validator');

const userSchema = new Schema({
    first_name: {
        type: String,
        lowercase: true,
        required: true,
    },
    last_name: {
        type: String,
        lowercase: true,
        required: true,
    },
    username: {
        type: String,
        lowercase: true,
        unique: true,
        required: true,
        trim: true
    },
    email: {
        type: String,
        lowercase: true,
        unique: true,
        required: true,
        trim: true,
        validate:{
            validator: validator.isEmail,
            message: 'Email is not a valid email',
            isAsync: false
        }
    },
    password: {
        type: String,
        minLength: 8,
        required: true,
        trim: true
    }
},
{
    timestamps: true
});

userSchema.pre(
    'save',
    async function(next) {
        const hash = await bcrypt.hash(this.password, 10);
        this.password = hash;
        next();
    }
); 

userSchema.methods.isValidPassword = async function(password, userPassword) {
    const compare = await bcrypt.compare(password, userPassword);  
    return compare;
};
  
const User = model('users', userSchema);

module.exports = User;