const express = require("express");
const dotenv = require("dotenv").config({ path: "./Configuration/config.env" });
const cors = require("cors");
const logger = require("./Middleware/winston");
const morgan = require("morgan");
const helmet = require("helmet");
const connectDB = require("./Database/mongoose");

const app = express();
const cors_whitelist = ["http://localhost:5000/"];
connectDB();

app.enable("trust proxy");

app.use(cors({
    origin: (origin, callback) => {
       cors_whitelist.indexOf(origin) !== -1 || !origin ?
            callback(null, true) :
            callback(new Error("Site Origin is not allowed by CORS"))
    },
    methods: "GET,PUT,PATCH,POST",
    preflightContinue: false,
    optionsSuccessStatus: 204,
}));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(morgan(':date[web] :remote-addr :method :url :status :res[content-length] - :response-time ms'));
app.use(helmet());

app.listen(process.env.PORT || 5000, () =>
    console.log(`Running at port http://localhost:${process.env.PORT || 5000}/`)
);

app.use("/authentication",require("./Routes/user.authentication"))
app.use(require("./Controller/error_handler").notFound);
app.use(require("./Controller/error_handler").errorHandler);