const logger = require("../Middleware/winston");

const notFound = (req, res, next) => {
    const error = new Error(`EndPoint Not Found - ${req.originalUrl}`);
    error.status(404);
    next(error);
};
  
const errorHandler = (error, req, res, next) => {
    const statusCode = error.statusCode === 502 ? 502 : error.statusCode;
    statusCode === 404 ? logger.warn(error, { metadata: error.stack }) : logger.error(error, { metadata: error.stack });
    res.status(statusCode);
    res.json({
        message: error.message,
        stack: error.stack,
    });
};
  
module.exports = { notFound, errorHandler };