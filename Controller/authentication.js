const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const User = require("../Model/schema.user");

exports.authSignIn = async (req, res, next) => {
    try {
        // let hashedPassword = await bcrypt.hash(req.body.password, 13);
        // const newUser = await User.create({ 
        //     first_name: req.body.first_name,
        //     last_name: req.body.last_name,
        //     username: req.body.username,
        //     email: req.body.email,
        //     password: hashedPassword
        // });
        let findUser = await User.findOne(
            { username: req.body.username }
        );
        if(findUser === null){
            let error = new Error('Username is not found');
            error.statusCode = 501;
            throw error;
        }
        res.status(200)
        res.send({
            status: findUser
            // status: newUser
        })
    } catch(err) {
        const error = new Error (err);
        error.statusCode = 401;
        return next(error);
    }
};
