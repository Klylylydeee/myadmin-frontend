const express = require("express");
const router = express.Router();
const authController = require("../Controller/authentication");

router.post('/sign-in', authController.authSignIn);
  
module.exports = router;